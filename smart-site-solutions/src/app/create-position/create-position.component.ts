import { Component, OnInit } from '@angular/core';
import { Position } from '../position';
import { Positions } from '../mock-positions';

import { Router } from '@angular/router';



@Component({
  selector: 'app-create-position',
  templateUrl: './create-position.component.html',
  styleUrls: ['./create-position.component.css']
})
export class CreatePositionComponent implements OnInit  {


  isShowDiv = true;

  constructor(private router:Router) { }

  arr_name:any[][]=[  ]



  position: Position={
    index: Positions.length,
    lat: -1,
    lng: -1,
    time: '2019-01-24T10:00:00Z',
  }

  ngOnInit(): void {

  }

 

// positionen speichen
  save() : void{
    Positions.push(this.position)

    //sort the time
    Positions.sort(function(a, b) {
      return (a.time < b.time) ? -1 : ((a.time > b.time) ? 1 : 0);
  });

    this.router.navigate(['positions'])

  }



//sort methode
  sortArray(): void {
    Positions.sort(function(a, b) {
      return (a.time < b.time) ? -1 : ((a.time > b.time) ? 1 : 0);
  });
  }
 

  calculate( ) {
    

    this.isShowDiv = !this.isShowDiv;
    
     //aufruf der sort methode um richtige werte zu berrchnen 
    this.sortArray()


  for(let i = 0; i < Positions.length - 1; i++) {

    //verwendete Fornmel https://www.kompf.de/gps/distcalc.html


    var dy_ = 111.3 * (Positions[i].lat - Positions[i + 1].lat)

    var lat_ = (Positions[i].lat + Positions[i + 1].lat) / 2 * 0.01745

    var dx_ = 111.3 * Math.cos(lat_) * (Positions[i].lng - Positions[i + 1].lng)

    var distance_ = Math.sqrt(dx_ * dx_ + dy_ * dy_)

    var distance_ = distance_ * 1000 

    var distanceRounded: number =+distance_.toFixed(1);
  

    //time differenz berechnen
    var startTime = Positions[i].time.toString()
    var endTime = Positions[i+1].time.toString()

    var starTimeCalc = new Date(startTime)
    var endTimeCalc = new Date(endTime)

    var sBetweenPos = endTimeCalc.valueOf() - starTimeCalc.valueOf();

    sBetweenPos= sBetweenPos * 0.001


    //geschwindigkeit berechnen
    var duration = distanceRounded / sBetweenPos
    var durationRounded: number =+duration.toFixed(1)

    //pushen in array um auszulesen
    this.arr_name.push([startTime,endTime,distanceRounded, durationRounded,sBetweenPos])

}


//console.log(this.arr_name)



  }


}

