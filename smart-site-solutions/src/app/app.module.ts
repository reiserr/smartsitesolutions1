import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { PositionsComponent } from './positions/positions.component';
import { AppRoutingModule } from './app-routing.module';
import { CreatePositionComponent } from './create-position/create-position.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    AppComponent,
    PositionsComponent,
    CreatePositionComponent,

    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
