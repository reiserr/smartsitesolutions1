import { Component, OnInit } from '@angular/core';

import { Positions } from '../mock-positions';
import { Position } from '../position';

@Component({
  selector: 'app-positions',
  templateUrl: './positions.component.html',
  styleUrls: ['./positions.component.css']
})
export class PositionsComponent implements OnInit {

  constructor() { }
  positionEntrys = 'Positions ';


  positions= Positions;
    

  ngOnInit(): void {
  }

//löschen von positionen 
  delete(position:Position): void{


    var index= Positions.indexOf(position)
    Positions.splice(index,1)
  }


  //sort
  sortArray(): void {
    Positions.sort(function(a, b) {
      return (a.time < b.time) ? -1 : ((a.time > b.time) ? 1 : 0);
  });
  }
 

}