import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';

import { PositionsComponent } from './positions/positions.component';
import { CreatePositionComponent } from './create-position/create-position.component';

const routes: Routes =[

{
  path:'positions',component:PositionsComponent
}, 
{
  path:'', component: CreatePositionComponent
}





];



@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
    
  ],exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
