export interface Position{
    
    index:number;
    lat: number;
    lng: number;
    time: String;
    
}